var express = require('express'),
    app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var cool = require('cool-ascii-faces');
//Set Env
app.set('port', (process.env.PORT || 5000));
app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(req, res){
  res.sendFile(__dirname + '/views/pages/index.html');
});

io.on('connection', function(socket){
  // Event fpr sending message, data being sent from client is recived here.
  socket.on('send message', function(msg) {
    io.emit('send message', msg)
  });
});

http.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
